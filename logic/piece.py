from .constants import *

class Piece:
    PADDING = 15
    OUTLINE = 2

    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color
        self.king = False
        self.x = 0
        self.y = 0
        self.calc_pos()
    
    def calc_pos(self):
        self.x = SQ_SIZE * self.col + SQ_SIZE // 2
        self.y = SQ_SIZE * self.row + SQ_SIZE // 2

    def make_king(self):
        self.king = True

    def draw(self, screen):
        radius = SQ_SIZE // 2 - self.PADDING
        pygame.draw.circle(screen, GREY, (self.x, self.y), radius + self.OUTLINE)
        pygame.draw.circle(screen, self.color, (self.x, self.y), radius)
        for i in range(3, self.PADDING // 2):
            pygame.draw.circle(screen, GREY, (self.x, self.y), radius - i * self.OUTLINE, radius - (i+5) * self.OUTLINE)
        pygame.draw.circle(screen, self.color, (self.x, self.y), radius - 5 * self.OUTLINE, radius - 6 * self.OUTLINE)
        if self.king:
            screen.blit(QUEEN, (self.x - QUEEN.get_width()//2, self.y - QUEEN.get_height()//2))

    def move(self, row, col):
        self.row = row
        self.col = col
        self.calc_pos()

    def __repr__(self):
        return str(self.color)