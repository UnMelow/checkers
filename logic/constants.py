import pygame

WIDTH = HEIGHT = 800
ROWS = COLS = 8
SQ_SIZE = WIDTH // COLS

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREY = (128, 128, 128)
BROWN = (69, 0, 20)

punkts = [(120, 140, u'Game', WHITE, BROWN, 0),
          (130, 210, u'Hard', WHITE, BROWN, 1),
          (120, 280, u'Peaceful', WHITE, BROWN, 2),
          (130, 350, u'Quit', WHITE, BROWN, 3)]

QUEEN = pygame.transform.scale(pygame.image.load('img/crown.png'), (45, 25))
