from logic.constants import *
from logic.game import Game
from simple_ai.minimax import *
import sys
import time


class Menu:
    def __init__(self, punkts = [120, 140, u'Punkt', WHITE, BROWN, 0]):
        self.punkts = punkts
        self.level = 4

    def render(self, poverhnost, font, num_punkt):
        for i in self.punkts:
            if num_punkt == i[5]:
                poverhnost.blit(font.render(i[2], 1, i[4]), (i[0], i[1]))
            else:
                poverhnost.blit(font.render(i[2], 1, i[3]), (i[0], i[1]))

    def menu(self):
        done = True
        font_menu = pygame.font.SysFont('arial', 50)
        punkt = 0
        while done:
            window.fill(GREY)
            mp = pygame.mouse.get_pos()
            for i in self.punkts:
                if mp[0] > i[0] and mp[0] < i[0] + 155 and mp[1] > i[1] and mp[1] < 50:
                    punkt = i[5]
            self.render(window, font_menu, punkt)

            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    sys.exit()
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        sys.exit()
                    if e.key == pygame.K_UP:
                        if punkt > 0:
                            punkt -= 1
                    if e.key == pygame.K_DOWN:
                        if punkt < len(self.punkts) - 1:
                            punkt += 1

                if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
                    if punkt == 0:
                        done =  False
                    elif punkt == 3:
                        sys.exit()
                    elif punkt == 1:
                        self.level = 4
                        done = False
                    else:
                        self.level = 2
                        done = False
            screen.blit(window, (0, 0))
            pygame.display.flip()

    def restart(self, wine):
        window.fill(WHITE)
        font = pygame.font.SysFont('arial', 50)
        if wine:
            window.blit((u'YOU WIN', 1, GREY), (300, 300))
        else:
            window.blit(font.render(u'YOU LOSE', 1, GREY), (300, 300))
        screen.blit(window, (0, 0))
        pygame.display.flip()

FPS = 60
pygame.init()
pygame.mixer.init()
window = pygame.Surface((800, 800))
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Checkers')


def get_pos(pos):
    x, y = pos
    row = y // SQ_SIZE
    col = x // SQ_SIZE
    return row, col


def main():
    """Creating menus and preparing to launch"""
    menu = Menu(punkts)
    menu.menu()
    run = True
    clock = pygame.time.Clock()
    game = Game(screen)

    while run:
        clock.tick(FPS)
        """event processing"""
        if game.winner() != None:
            run = False
            if game.winner() == WHITE:
                menu.restart(True)
            else:
                menu.restart(False)
            time.sleep(5)
            main()

        if game.turn == BLACK:
            value, new_board = minimax(game.get_board(), menu.level, BLACK, game)
            game.ai_move(new_board)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                row, col = get_pos(pos)
                game.select(row, col)

        game.update()
    pygame.quit()


if __name__ == "__main__":
    main()